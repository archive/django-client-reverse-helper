import apiRequest from './api-requests';

export default function (ident, callback, args, kwargs) {
  return apiRequest.post('/reverse/', {ident, args, kwargs});
};
