import * as request from 'superagent';

export default {
  post: function (url, params) {
    return request
      .post(url)
      .send(params)
      .set('X-CSRF-TOKEN', window.CSRF_TOKEN)
      .set('Accept', 'application/json');
  },
};
