/*global describe, it, beforeEach */
import { expect } from 'chai';
import { spy, stub } from 'sinon';

var superagent = require('superagent');
var superagentStub = require('./__mocks__/superagent');

stub(superagent, superagentStub);

describe('Reverser', function () {

  var Reverse, apiRequests;

  beforeEach(function () {
    Reverse = require('./../app');
    apiRequests = require('./../api-requests');
  });

  it('should get results', function () {
    var data = 'thing:thing';
    superagent.__setResponse({status: 200, ok: 'ok', body: data});
    spy(apiRequests, 'post');

    Reverse(data);

    expect(apiRequests.post).to.have.been.calledWith('/reverse/');
  });
});
