/* global jest */

var superagent = jest.genMockFromModule('superagent');

var MockResponse = jest.genMockFunction().mockImplementation(function () {
  this.status = 200;
  this.ok = true;
});

MockResponse.prototype.send = jest.genMockFunction();
MockResponse.prototype.toError = jest.genMockFunction();

var MockRequest = jest.genMockFunction().mockImplementation(function (method, url) {
  this.method = method;
  this.url = url;
});

MockRequest.prototype.accept = jest.genMockFunction().mockReturnThis();
MockRequest.prototype.set = jest.genMockFunction().mockReturnThis();
MockRequest.prototype.send = jest.genMockFunction().mockReturnThis();
MockRequest.prototype.field = jest.genMockFunction().mockReturnThis();
MockRequest.prototype.query = jest.genMockFunction().mockReturnThis();
MockRequest.prototype.end = jest.genMockFunction().mockImplementation(function (callback) {

  if (superagent.mockDelay) {
    this.delayTimer = setTimeout(callback, 0, superagent.mockError, superagent.mockResponse);

    return;
  }

  callback(superagent.mockError, superagent.mockResponse);
});

MockRequest.prototype.abort = jest.genMockFunction().mockImplementation(function () {
  this.aborted = true;

  if (this.delayTimer) {
    clearTimeout(this.delayTimer);
  }
});

superagent.Request = MockRequest;
superagent.Response = MockResponse;

superagent.mockResponse = new MockResponse();
superagent.mockError = null;
superagent.mockDelay = false;

function __setResponse (options) {
  var status = typeof options.status !== 'undefined' ? options.status : 200;
  var ok = typeof options.ok !== 'undefined' ? options.ok : true;
  var body = typeof options.body !== 'undefined' ? options.body : {};
  var error = typeof options.error !== 'undefined' ? options.error : {};

  var mockedResponse = jest.genMockFunction().mockImplementation(function () {
    this.status = status;
    this.ok = ok;
    this.body = body;
  });

  superagent.mockError = {
    response: {
      body: error
    }
  };

  superagent.mockResponse = new mockedResponse();
}

//module.exports = superagent;

module.exports = {
  Response: MockResponse,
  Request: MockRequest,
  _requests: [],
  _newRequest: function () {
    var r = new MockRequest();
    this._requests.push(r);
    return r;
  },
  post: function () {
    return this._newRequest();
  },
  get: function () {
    return this._newRequest();
  },
  put: function () {
    return this._newRequest();
  },
  __setResponse: __setResponse
};
